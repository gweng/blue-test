##Design

Currently, the service scales at 3 levels: the hardware level, the web server level, and the worker level. On the hardware level, 'throng' is the utility I used to take full advantage of multiple cpus. (If I am not running on Heroku, I would use pm2 so I have more control). On the web server level, you can scale up as the number of connection increases by adding more instances. On the worker level, you can scale up as the worker queue builds up and you need to digest the workload to maintain a certain performance level.

An added benefit of the message queue is that the worker can potentially be implemented in different languages as long as it can connect to the queue.

Having the web servers and the data workers running on different machines is a good idea, because they have different behaviours, which affects their memory usage patterns. For example, web servers are more for serving static contents or handling real time traffic, whereas the data workers will talk to the database and carry out heavy calculations. Therefore, it would be more efficient to keep them on separate machines.

In terms of the project structure, it's been split up into four parts:

	/api - for business logic
	/lib - for the web server
	/services - for the workers
	/test - for tests

db.js provides an interface to access the database methods so swapping out the database means rewriting just the db.js file.

I wanted to have a microservice architecture so it's easier to scale out and rabbitmq is a popular choice for the message queue. It's supported commercially, open source, and battle tested. MongoDB scales out horizontally and has a build-in MapReduce. I thought the tag service call would land well using a MapReduce approach.

##Assumptions
- the id for an article is created by the service instead of specified by the caller of the service
- /tag/{tagName}/{date} returns "articles" that contains the last 10 articles with the tag entered for that day. It makes more sense for this api call, rather than the last 10 regardless of the tag.

## Local dependencies
- [RabbitMQ](http://www.rabbitmq.com/) for job queueing
- [MongoDB](https://www.mongodb.com/) for database

## Installing on Mac
Install NVM and Node 7
https://github.com/creationix/nvm

RabbitMQ and MongoDB via brew (Install Homebrew first if needed)
```
brew install mongodb
brew install rabbitmq
rabbitmq-server
mongod --dbpath=./
npm install
```

## Running
1. `npm start`
2. [http://localhost:5000](http://localhost:5000) is where the service lives
3. For example, POST to http://localhost:5000/articles

#### Locally
`npm start` runs [node-foreman](http://strongloop.github.io/node-foreman/), which will check the [Procfile]
and start a single web process and a single worker process. (You can also run 'node server.js' and 'node services/worker.js')

## Architecture

The api service is divided into a worker and a web api. 
This microservice architecture allows multiple workers to work on the queue.
It provides a separation of concerns as well as making it easier to scale out gradually.

## Testing
`npm test`

api-integration.test is end to end, whereas article.test is just testing the api logic layer

## Sample requests and responses

### Request to add a new article to the database
POST http://localhost:5000/articles
```
{
  "title": "latest science shows that potato chips are better for you than sugar",
  "date" : "2016-09-22",
  "body" : "some text, potentially containing simple markup about how potato chips are great",
  "tags" : ["health", "fitness", "science"]
}
```
Response:
```
{
  "response": {
    "title": "latest4 science shows that potato chips are better for you than sugar",
    "date": "2016-09-22",
    "body": "some text, potentially containing simple markup about how potato chips are great",
    "tags": [
      "health",
      "fitness",
      "science"
    ],
    "id": "58ef69cb3b73b7bd33597a1e"
  }
}
```


### Request to get an article in the database
GET http://localhost:5000/articles/58ef69cb3b73b7bd33597a1e

Response:
```
{
  "response": {
    "title": "latest science shows that potato chips are better for you than sugar",
    "date": "2016-09-22",
    "body": "some text, potentially containing simple markup about how potato chips are great",
    "tags": [
      "health",
      "fitness",
      "science"
    ],
    "id": "58ef69cb3b73b7bd33597a1e"
  }
}
```

### Request to query articles by a specific tag and date
GET http://localhost:5000/tags/health/20160922

```
{
  "response": {
    "tag": "health",
    "count": 2,
    "articles": [
      "58ef5b84a2f1cd9e5917ad0d",
      "58ef69cb3b73b7bd33597a1e"
    ],
    "related_tags": [
      "fitness",
      "science"
    ]
  }
}
```

Author: Gary Weng (gweng87@gmail.com)