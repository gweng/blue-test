var should = require('should');
var articleApi = require('../api/article');

const dbMock = {
    save: (_, article) => {
        return new Promise((resolve, reject) => {
            resolve(article);
        });
    },
    getById: (_, id) => {
        return new Promise((resolve, reject) => {
            if(id === '1') {
                resolve({
                    "title": "latest science shows that potato chips are better for you than sugar",
                    "date": "2016-09-22",
                    "body": "some text, potentially containing simple markup about how potato chips are great",
                    "tags": ["health", "fitness", "science"]
                });
            }else{
                resolve(undefined) // not found
            }
        });
    }
};

// test just the api layer
describe('ArticleApi', function () {

    it('add an article should get the article back with id', function (done) {
        var req = {
            "title": "latest science shows that potato chips are better for you than sugar",
            "date": "2016-09-22",
            "body": "some text, potentially containing simple markup about how potato chips are great",
            "tags": ["health", "fitness", "science"]
        };

        var answer = {
            "title": "latest science shows that potato chips are better for you than sugar",
            "date": "2016-09-22",
            "body": "some text, potentially containing simple markup about how potato chips are great",
            "tags": ["health", "fitness", "science"]
        };

        articleApi.save(dbMock, req, function (err, article) {
            article.should.eql(answer);
            done();
        });
    });

    it('add an article with empty tag should get the article back with id', function (done) {
        var req = {
            "title": "latest science shows that potato chips are better for you than sugar",
            "date": "2016-09-22",
            "body": "some text, potentially containing simple markup about how potato chips are great",
            "tags": []
        };

        var answer = {
            "title": "latest science shows that potato chips are better for you than sugar",
            "date": "2016-09-22",
            "body": "some text, potentially containing simple markup about how potato chips are great",
            "tags": []
        };

        articleApi.save(dbMock, req, function (err, article) {
            article.should.eql(answer);
            done();
        });
    });

    it('get an article by id', function (done) {
        var answer = {
            "title": "latest science shows that potato chips are better for you than sugar",
            "date": "2016-09-22",
            "body": "some text, potentially containing simple markup about how potato chips are great",
            "tags": ["health", "fitness", "science"]
        };

        articleApi.getById(dbMock, '1', function (err, article) {
            article.should.eql(answer);
            done();
        });
    });

});