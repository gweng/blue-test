var should = require('should');
var request = require('superagent');
var server = require('../server');
var api = require('../services/worker');

describe('API integration', function () {

    before(function (done) {
        server.start(function () {
            api.start(done);
        });

    });

    it('empty object should return error message', function (done) {

        request.post('http://localhost:5000/articles', '', function (err, res) {
            err.status.should.equal(400);
            res.body.should.eql({error: 'Could not decode request:Empty or invalid input message.'});
            done();
        });

    });

    it('sample request should work', function (done) {
        var sampleReq = {
            "title": "latest science shows that potato chips are better for you than sugar",
            "date": "2016-09-22",
            "body": "some text, potentially containing simple markup about how potato chips are great",
            "tags": ["health", "fitness", "science"]
        };
        var answer = {
            response: {
                "title": "latest science shows that potato chips are better for you than sugar",
                "date": "2016-09-22",
                "body": "some text, potentially containing simple markup about how potato chips are great",
                "tags": ["health", "fitness", "science"]
            }
        };

        request.post('http://localhost:5000/articles', sampleReq, function (err, res) {
            res.status.should.equal(200);
            res.body.response.id.should.not.be.null();
            answer.response.id = res.body.response.id;
            res.body.should.eql(answer);
            done();
        });

    });


});