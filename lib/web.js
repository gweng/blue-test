const express = require('express');
const jade = require('jade');
const path = require('path');
const parallel = require('./parallel');
const logger = require('logfmt');
const bodyParser = require('body-parser');

var articles = require('../api/article');

module.exports = function web(rabbit, serviceTime) {

  var app = express();

  app.set('view engine', 'jade');
  app.set('view cache', true);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  function customErrorHandler(err, req, res, next) {
    if (err && err.status === 400) {
      sendErrorResponse(res, 400, err.message);
    } else {
      next();
    }
  }

  //catch the json parsing error
  app.use(customErrorHandler);

  //api routes
  app.post('/articles', articlePost, sendResponse);
  app.get('/articles/:id', articleGet, sendResponse);
  app.get('/tags/:tagName/:date', tagInfoGet, sendResponse);

  //catch other errors
  app.use(customErrorHandler);

  const exchange = rabbit.default();
  let channelInitialized = false; // for api test to make sure the channel has been created before publish
  function articlePost(req, res, next) {

    if (!req.body) {
      next({ status: 400, message: 'Empty or invalid input message.' });
    } else {
      const queueKey = 'article.post';
      const rpc = exchange.queue({ name: queueKey, prefetch: 1 })

      // logger.log({ type: 'debug', message: 'publish payload', body: JSON.stringify(req.body) });
      exchange.publish({ reqBody: req.body }, { key: queueKey, reply: onReply(res, next) });
    }
  }

  function articleGet(req, res, next) {
    if (!req.params.id) {
      next({ status: 400, message: 'Missing id.' });
    } else {
      const queueKey = 'article.get';
      const rpc = exchange.queue({ name: queueKey, prefetch: 1 })

      //logger.log({ type: 'debug', message: 'publish payload', body: JSON.stringify(req.body) });
      exchange.publish({ id: req.params.id }, { key: queueKey, reply: onReply(res, next) });
    }
  }

  function tagInfoGet(req, res, next) {

    if (!req.params.tagName || !req.params.date) {
      next({ status: 400, message: 'Missing id.' });
    } else {
      const queueKey = 'taginfo.get';
      const rpc = exchange.queue({ name: queueKey, prefetch: 1 })

      //logger.log({ type: 'debug', message: 'publish payload', body: JSON.stringify(req.body) });
      exchange.publish({
        tagName: req.params.tagName,
        date: req.params.date
      }, { key: queueKey, reply: onReply(res, next) });
    }
  }

  //helpers
  const onReply = (res, next) => result => {
    if (result.status === 200) {
      res.locals.payload = result.message;
      next();
    } else {
      next(result);
    }
  };

  function sendResponse(req, res, next) {
    if (res.locals.payload) { //all good
      res.json({ response: res.locals.payload });
    } else {
      //probably all workers are down, no one is listening to the queue
      sendErrorResponse(res, 503, 'The service is down at the moment, please try again later.');
    }
  }

  function sendErrorResponse(res, statusCode, message) {
    res.status(statusCode).json({ error: "Could not decode request:" + message });
  }

  return app;
};
