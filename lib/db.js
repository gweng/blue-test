const logger = require('logfmt');
const mongodb = require('mongodb');
const ObjectID = mongodb.ObjectID;

module.exports = {

  init: (db) => {
    this._db = db
  },

  save: (collectionName, data) => {
    const collection = this._db.collection(collectionName);

    return collection
      .insertOne(data)
      .then(result => {
        logger.log({ type: 'info', message: 'insert doc into collection.', data, collection });
        data.id = result.insertedId;
        delete data._id; // part of the requirement to have "id", mongodb ObjectId is a timestamp
        return data;
      })
      .catch(err => {
        logger.log({ type: 'error', message: 'DB server save error.', err });
      });
  },

  getById: (collectionName, _id) => {
    const collection = this._db.collection(collectionName);

    return collection
      .find({ _id: ObjectID(_id) })
      .toArray()
      .then(results => {
        if (results.length === 0) {
          return { result: 'no results' };
        }

        results[0].id = results[0]._id;
        delete results[0]._id;

        return results[0];
      })
      .catch(err => {
        logger.log({ type: 'error', message: 'DB server save error.', err });
      });
  },

  getTagInfo: (collectionName, tag, date) => {
    const collection = this._db.collection(collectionName);

    //using mapReduce to consolidate all the ids and tags
    return collection
      .mapReduce(
        function () { //map
          emit(this.date, { id: this._id, tags: this.tags });
        },
        function (key, values) { //reduce
          const reducedVal = { ids: {}, tags: {} };
          for (var i = 0; i < values.length; i++) {
            reducedVal.ids[values[i].id.valueOf()] = 1;
            values[i].tags.forEach(function (t) {
              reducedVal.tags[t] = 1;
            })
          }
          return reducedVal;
        },
        {
          out: "tag_date_info_MR",
          query: {
            date: { $eq: date },
            tags: { $elemMatch: { $eq: tag } }
          },
          sort: { date: 1 }
        })
      .then(colection => colection.find().toArray())
      .then(results => {
        if (results.length === 0) {
          return { result: 'no results' };
        }
        // console.log('result', results)
        const val = results[0].value;

        let ids, related_tags;

        if (val.ids) {
          ids = Object.keys(val.ids);
          delete val.tags[tag]; //don't show the root tag
          related_tags = Object.keys(val.tags);
        } else {
          //val.ids is undefined when there is only one entry found, because reducer didn't run
          ids = [val.id.valueOf()];
          related_tags = val.tags.filter(t => t !== tag);
        }

        const res = {
          tag,
          count: ids.length,
          articles: ids.slice(-10), //objectids are sorted by time,
          related_tags
        };

        return res;
      })
      .catch(err => {
        logger.log({ type: 'error', message: 'DB server save error.', err });
        if (err.errmsg === "ns doesn't exist") {
          return { result: 'no results' };
        }
      });
  },

  close: () => {
    this._db.close();
  }
};

