const http = require('http');
const throng = require('throng');
const cpus = require('os').cpus().length;
const logger = require('logfmt');
const jackrabbit = require('jackrabbit');

http.globalAgent.maxSockets = Infinity;

const web = require('./lib/web');

const RABBIT_URL = process.env.CLOUDAMQP_URL || 'amqp://localhost';
const PORT = process.env.PORT || 5000;
const SERVICE_TIME = process.env.SERVICE_TIME || 2000;

throng({ workers: cpus, lifetime: Infinity, start });

function start(next) {

  logger.log({ type: 'info', message: 'starting server' });
  const broker = jackrabbit(RABBIT_URL, 1);
  let server;

  broker.once('connected', listen);
  broker.once('disconnected', exit.bind(this, 'disconnected'));
  process.on('SIGTERM', exit);

  function listen() {
    var app = web(broker, SERVICE_TIME);
    server = http.createServer(app)
    server.listen(PORT, next);
    logger.log({ type: 'info', message: 'listening on port: ' + PORT });
  }

  function exit(reason) {
    logger.log({ type: 'info', message: 'closing server', reason: reason });
    db.close();
    if (server) server.close(process.exit.bind(process));
    else process.exit();
  }

}

//for api test
module.exports.start = start;
