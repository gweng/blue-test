const assert = require('assert');

module.exports = {

  save: function (db, payload, next) {
    // data validation
    assert(payload.title);
    assert(payload.date);
    assert(payload.body);
    assert(payload.tags && Array.isArray(payload.tags) && payload.tags.every(t => (typeof t === 'string')));

    return db.save('articles', payload)
      .then(result => {
        next(null, result)
      })
  },

  getById: function(db, id, next) {
    assert(id);

    return db.getById('articles', id)
      .then(result => {
        next(null, result)
      })
  },

  getTagInfo: function(db, tag, date, next) {
    assert(tag);
    assert(date && /\d{8}/.test(date));
    const formatDate = date.substr(0,4) + '-' + date.substr(4,2) + '-' + date.substr(6,2);

    return db.getTagInfo('articles', tag, formatDate)
      .then(result => {
        next(null, result);
      })
  }

};

