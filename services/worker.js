const jackrabbit = require('jackrabbit');
const logger = require('logfmt');
const throng = require('throng');
const cpus = require('os').cpus().length;
const article = require('../api/article');
const _ = require('lodash');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const url = process.env.MONGOLAB_URI || 'mongodb://localhost:27017/mydb';
const DB = require('../lib/db');

const RABBIT_URL = process.env.CLOUDAMQP_URL || 'amqp://localhost';

throng({ workers: cpus, lifetime: Infinity, start });

function start(next) {
  logger.log({ type: 'info', message: 'starting worker' });

  const rabbit = jackrabbit(RABBIT_URL);
  const exchange = rabbit.default();
  const rpcPost = exchange.queue({ name: 'article.post', prefetch: 1 });
  const rpcGet = exchange.queue({ name: 'article.get', prefetch: 1 });
  const rpcTagInfo = exchange.queue({ name: 'taginfo.get', prefetch: 1 });
  let _db;

  //for api integration test
  rabbit.on('connected', () => {
    if(typeof next === 'function') next(); //worker is ready
  });

  MongoClient
    .connect(url)
    .then(mongo => {
      DB.init(mongo)

      rpcPost.consume((req, reply) => {
        if (_.isEmpty(req.reqBody)) {
          reply({ status: 400, message: 'Empty or invalid input message.' });
        } else {
          article.save(DB, req.reqBody, function (err, article) {
            if (err) return reply({ status: 400, message: err });

            reply({ status: 200, message: article });
          });
        }
      });

      rpcGet.consume((req, reply) => {
        if (!req.id) {
          reply({ status: 400, message: 'Missing id.' });
        } else {
          article.getById(DB, req.id, function (err, article) {
            if (err) return reply({ status: 400, message: err });

            reply({ status: 200, message: article });
          });
        }
      });

      rpcTagInfo.consume((req, reply) => {
        if (!req.tagName || !req.date) {
          reply({ status: 400, message: 'tag and date are required.' });
        } else {
          article.getTagInfo(DB, req.tagName, req.date, function (err, article) {
            if (err) return reply({ status: 400, message: err });

            reply({ status: 200, message: article });
          });
        }
      });


    })
    .catch(err => {
      logger.log({ type: 'error', message: 'Unable to connect to the mongoDB server.', err });
    });

  process.once('uncaughtException', onError);
  function onError(err) {
    logger.log({ type: 'error', service: 'article', error: err, stack: err.stack || 'No stacktrace' }, process.stderr);
    logger.log({ type: 'info', message: 'killing worker' });
    process.exit();
  }

  process.on('SIGTERM', exit);
  function exit(reason) {
    logger.log({ type: 'info', message: 'closing worker', reason: reason });
    _db.close();
    process.exit();
  }
}

//for api test
module.exports.start = start;